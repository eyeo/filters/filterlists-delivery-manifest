#!/bin/sh -e

usage() {
  echo "Usage: $0 [fixture string] [path to artifacts] [path to manifest]"
}

if [ "$1" = "--help" -o  "$1" = "-h" ]; then
	usage
	exit 0
fi

if [  $# -le 2 ]; then
  echo "Not enough arguments"
  usage
  exit 1
fi

if [[ "$1" =~ ^[+-]?[0-9]+\.$ ]]; then
  echo "$1 not a fixture, aiee" >&2
  usage
  exit 1
fi
fixture="$1"

if [ ! -d "$2" ]; then
  echo "$2 not a directory, aiee" >&2
  usage
  exit 1
fi
artifacts="$2"

if [ ! -e "$3" ]; then
  echo "$3 does not exist" >&2
  usage
  exit 1
fi
manifest="$3"

set +e
./src/manifest_sanity_check.sh $artifacts $manifest > ./test/tmp.txt
status=$?

if  [[ $status -eq 1 ]]
then
  message="$(grep -F "$fixture" ./test/tmp.txt)"
  rm ./test/tmp.txt
  if [[ "$message" = "$fixture" ]]
  then
    exit 0
  else
    exit 1
  fi
else
  exit 1
fi
