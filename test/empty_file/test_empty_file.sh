#!/bin/sh -e

fixture="Error: test/empty_file/artifacts/c.txt: File is empty"
artifacts=test/empty_file/artifacts
manifest=test/empty_file/manifest.json

./test/expect_error.sh "$fixture" $artifacts $manifest
