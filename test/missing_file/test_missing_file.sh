#!/bin/sh -e

fixture="Error: test/missing_file/artifacts/c.txt: Missing file"
artifacts=test/missing_file/artifacts
manifest=test/missing_file/manifest.json

./test/expect_error.sh "$fixture" $artifacts $manifest
