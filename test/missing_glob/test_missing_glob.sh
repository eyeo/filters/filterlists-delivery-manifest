#!/bin/sh -e

fixture="ls: test/missing_glob/artifacts/dir/*.json: No such file or directory"
artifacts=test/missing_glob/artifacts
manifest=test/missing_glob/manifest.json

./test/expect_error.sh "$fixture" $artifacts $manifest
