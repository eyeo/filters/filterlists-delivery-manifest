#!/bin/sh -e

fixture="Error: test/invalid_json/artifacts/c.json: Invalid JSON"
artifacts=test/invalid_json/artifacts
manifest=test/invalid_json/manifest.json

./test/expect_error.sh "$fixture" $artifacts $manifest
