FROM alpine:latest

RUN apk update && apk add jq && rm -rf /var/cache/apk/*

RUN mkdir /app
COPY ./ /app

WORKDIR /app
