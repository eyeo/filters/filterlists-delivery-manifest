#!/bin/sh -e

display_usage() {
  echo "Usage: $0 [path to artifacts] [path to manifest]"
}

if [ "$1" = "--help" -o  $1 = "-h" ]; then
	display_usage
	exit 0
fi

if [  $# -le 1 ]; then
  echo "Not enough arguments"
  display_usage
  exit 1
fi

if [ ! -d "$1" ]; then
  echo "$1 not a directory, aiee" >&2
  display_usage
  exit 1
fi
artifacts="$1"

if [ ! -e "$2" ]; then
  echo "$2 does not exist" >&2
  display_usage
  exit 1
fi
manifest="$2"

# Sanity check files for existence, size > 0, JSON validity
# Only exit with an error if file is not set as flexible

output() {
  level=$([ "$2" = true ] && echo "Warning" || echo "Error")
  echo "$level: $1"
  if [ $2 = false ]; then
    exit 1
  fi
}

# Produces a list of pairs <pattern>:<isFlexible> for records of type "file"
records_files=$(jq -r '.records[] | select( .type == "file" ) | [.pattern, .isFlexible|tostring] | join(":")' "$manifest")

for record in $records_files; do
  # Split jq-extracted data with parameter substitution
  pattern=${record%:*}
  flexible=${record#*:}
  path="$artifacts/$pattern"
  
  # Does file exist
  if [ -e "$path" ]; then
    # Is file empty
    if [ $(wc -c < "$path") -eq 0 ]; then
      output "$path: File is empty" $flexible
    fi
    # Is JSON valid
    if [ "${path#*.}" = "json" ] && ! jq -e . >/dev/null 2>&1 < "$path"; then
    output "$path: Invalid JSON" $flexible
    fi
  else
    output "$path: Missing file" $flexible
  fi
done

# Sanity check directories

records_directories=$(jq -r '.records[] | select( .type == "directory" and .isFlexible == false) | .pattern' "$manifest")

for record in $records_directories; do
  ls "$artifacts"/$record  2> /dev/stdout 1> /dev/null
done
